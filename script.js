/*
	Von Arceo
	s33 Activity

*/

// No.3

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data)
})

// No. 4 (Stretch Goal)

// let map1 = [];

// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response) => response.json())
// .then((data) => {
// 	map1 = data.map((elem) => {
// 	return elem.title    

//   })
//      console.log(map1);
// })

// No.5

	fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((data) => { 
		console.log(data)
	})

// No.6

fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((data) => { 
		console.log('Title: ' + data.title)
		console.log('Status: ' + data.completed)
	})	

//No.7


	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Activity No. 7',
			body: 'this is to create a new post',
			userID: 1
		})
	})

	.then((response) => response.json())
	.then((data) => console.log(data))

// No.8

fetch('https://jsonplaceholder.typicode.com/todos/201', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Activity No. 8 Updated Title using PUT/PATCH',
			completed: false,

		})
	})

	.then((response) => response.json())
	.then((data) => console.log(data))	

// No.9

fetch('https://jsonplaceholder.typicode.com/todos/201', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Activity No. 9',
			description: 'New Description',
			status: true,
			dateComplete: '2022-10-03',
			usersID: '69'
		})
	})

	.then((response) => response.json())
	.then((data) => console.log(data))

// No.10

fetch('https://jsonplaceholder.typicode.com/todos/50', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Activity No. 10 Updated Title using PUT/PATCH',
			
		})
	})

	.then((response) => response.json())
	.then((data) => console.log(data))

// No.11


fetch('https://jsonplaceholder.typicode.com/todos/0', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Activity No. 11',
			dateComplete: '2022-10-03',
			completed: true
		})
	})

	.then((response) => response.json())
	.then((data) => console.log(data))

	// No.12

	fetch('https://jsonplaceholder.typicode.com/todos/201', {
	method: 'DELETE'
})

